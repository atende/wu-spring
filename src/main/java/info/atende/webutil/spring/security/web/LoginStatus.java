package info.atende.webutil.spring.security.web;

import java.io.Serializable;

/**
 * Classe representando um estado de login
 * Criado por Giovanni Candido <giovanni@atende.info>.
 * Data: 01/11/12
 * Hora: 16:34
 */
public class LoginStatus implements Serializable {
    private boolean success;
    private boolean loggedIn;
    private String username;
    private String errorMessage;
    private String redirectUrl;

    /**
     * Cria uma responsta de sucesso de login
     * @param success
     * @param loggedIn
     * @param username
     * @param errorMessage
     * @param redirectUrl
     */
    public LoginStatus(boolean success, boolean loggedIn, String username, String errorMessage, String redirectUrl) {
        this.success = success;
        this.loggedIn = loggedIn;
        this.username = username;
        this.errorMessage = errorMessage;
        this.redirectUrl = redirectUrl;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
