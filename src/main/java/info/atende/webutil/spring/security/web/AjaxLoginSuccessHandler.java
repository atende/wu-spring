package info.atende.webutil.spring.security.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Personalizar o login de usuário no Spring para ser compatível com chamadas Ajax, retornando JSON
 * Criado por Giovanni Candido <giovanni@atende.info>.
 * Data: 01/11/12
 * Hora: 16:44
 */
public class AjaxLoginSuccessHandler implements AuthenticationSuccessHandler {
    private String redirectUrl;
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        ObjectMapper mapper = new ObjectMapper();
        LoginStatus loginStatus = new LoginStatus(true, authentication.isAuthenticated(), authentication.getName(), null,
        redirectUrl);
        OutputStream outputStream = httpServletResponse.getOutputStream();
        mapper.writeValue(outputStream, loginStatus);
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
