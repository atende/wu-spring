package info.atende.webutil.spring.security.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Personaliza o processo de login para ser compativel com chamadas ajax como as chamadas do Extjs
 * Criado por Giovanni Candido <giovanni@atende.info>.
 * Data: 01/11/12
 * Hora: 16:26
 */
public class AjaxLoginFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        ObjectMapper mapper = new ObjectMapper();
        LoginStatus loginStatus = new LoginStatus(false, false, null, "Usuário ou senha incorretos, falha ao logar. " +
                "Tente novamente", null);
        OutputStream out = httpServletResponse.getOutputStream();
        mapper.writeValue(out, loginStatus);

    }
}
