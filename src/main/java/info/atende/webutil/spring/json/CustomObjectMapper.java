package info.atende.webutil.spring.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

import javax.annotation.PostConstruct;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Customizar a serializacao em json, usando JacksonMapper 2.0
 * Criado por Giovanni Candido <giovanni@atende.info>.
 * Data: 01/11/12
 * Hora: 16:59
 */
public class CustomObjectMapper extends ObjectMapper {
    private String dateMask = "MM-dd-yyyy HH:mm:ss";
    @PostConstruct
    public void afterPropertiesSet() throws Exception {
        /**
         * Para evitar hibernate lazy load exceptions
         */
        Hibernate4Module hm = new Hibernate4Module();
        registerModule(hm);
        configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        /**
         * Para serializacao de datas
         */
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        DateFormat dateFormat = new SimpleDateFormat(dateMask);
        setDateFormat(dateFormat);
    }
}
